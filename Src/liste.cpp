#include "liste.hpp"

#include <iostream>
#include <cassert>

Liste::Liste() {
  this->firstCell = nullptr;
}

Liste::Liste(const Liste& autre) {
  const Cellule * cell = autre.tete();
  Cellule* cellInit = new Cellule(autre.tete()->valeur);
  this->firstCell= cellInit;
  while(cell->next!=nullptr){
    cell=cell->next;    
    this->ajouter_en_queue(cell->valeur);   
    
  }

}

Liste& Liste::operator=(const Liste& autre) {
  /* votre code ici */
  return *this ;
}

Liste::~Liste() {
  /* votre code ici */
}

void Liste::ajouter_en_tete(int valeur) {
  Cellule* cell = new Cellule(valeur,this->firstCell);
  firstCell = cell;

}

void Liste::ajouter_en_queue(int valeur) {
  Cellule* cellInsert = new Cellule(valeur);
  Cellule* cell = this->firstCell;
  while(cell->next!=nullptr){
    cell=cell->next;
  }
  cell->next=cellInsert;
}

void Liste::supprimer_en_tete() {
  Cellule* cell = new Cellule;
  cell = firstCell;
  firstCell = cell->next;
  delete cell;
}

Cellule* Liste::tete() {
  Cellule* cell = new Cellule;
  cell = this->firstCell;
  return cell;
}

const Cellule* Liste::tete() const {
  Cellule* cell = new Cellule;
  cell = this->firstCell;
  return cell;
}

Cellule* Liste::queue() {
  Cellule * cell = this->firstCell;
  while(cell->next!=nullptr){
    cell=cell->next;
  }
  return cell;
}

const Cellule* Liste::queue() const { 
  const Cellule * cell = this->firstCell;
  while(cell->next!=nullptr){
    cell=cell->next;
  }
  return cell;
}

int Liste::taille() const {
  int retTaille =0;
  Cellule * cell = this->firstCell;
  while(cell!=NULL){
    retTaille=retTaille+1;
    cell=cell->next;
  }
  return retTaille;
}

Cellule* Liste::recherche(int valeur) {
  Cellule * cell = this->firstCell;
  bool present = false;
  while(!present && (cell!=nullptr)){
    if(cell->valeur==valeur){
      present = true;
    }else{
      cell=cell->next;
    }
  }
  if(present){
    return cell;
  }else{
    return nullptr;
  }
}

const Cellule* Liste::recherche(int valeur) const {
  Cellule * cell = this->firstCell;
  bool present = false;
  while(!present && (cell!=nullptr)){
    if(cell->valeur==valeur){
      present = true;
    }else{
      cell=cell->next;
    }
  }
  if(present){
    return cell;
  }else{
    return nullptr;
  }
}

void Liste::afficher() const {
  Cellule * cell = this->firstCell;
  std::cout<<"[ ";
  while(cell!=nullptr){
    std::cout<<cell->valeur;
    std::cout<<" ";
    cell=cell->next;
  }
  std::cout<<"]"<<std::endl;

}
