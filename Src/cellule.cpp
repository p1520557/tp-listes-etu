#include "cellule.hpp"
#include <cstddef>

Cellule::Cellule(int valeur){
	this->valeur=valeur;
	this->next=nullptr;

}
Cellule::Cellule(int valeur, Cellule* next){
	this->valeur=valeur;
	this->next=next;
}

Cellule::Cellule(){
	this->valeur=0;
	this->next=nullptr;
}
