#ifndef LIFAP6_LISTE_CELLULE_HPP
#define LIFAP6_LISTE_CELLULE_HPP



class Cellule {


public:

	int valeur;
	Cellule* next;
	Cellule();
	Cellule(int valeur);
	Cellule(int valeur, Cellule* next);
	int getValeur();


} ;

#endif
